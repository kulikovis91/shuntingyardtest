﻿using System;
using Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class SimpleCalculatorTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var calc = new SimpleCalculator();
            var foo = calc.Eval("5*(1+89/2)");
            Assert.AreEqual("227,5", foo);
        }

        [TestMethod]
        public void TestMethod2()
        {
            var calc = new SimpleCalculator();
            var foo = calc.Eval("8+6/4*2");
            Assert.AreEqual("11,0", foo);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var calc = new SimpleCalculator();
            var foo = calc.Eval("(5+3)/2,88");
            Assert.AreEqual("2,8", foo);
        }

        [TestMethod]
        public void TestMethod4()
        {
            var calc = new SimpleCalculator();
            var foo = calc.Eval("9+2/(7-3)");
            Assert.AreEqual("9,5", foo);
        }
    }
}
