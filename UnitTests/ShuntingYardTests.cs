﻿using System.Linq;
using Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ShuntingYardTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var infixExpression = "9+24/(7-3)";
            var foo = ShuntingYard.Shunt(infixExpression);
            Assert.AreEqual(7, foo.Count);
            Assert.AreEqual("9", foo[0]);
            Assert.AreEqual("24", foo[1]);
            Assert.AreEqual("7" , foo[2]);
            Assert.AreEqual("3" , foo[3]);
            Assert.AreEqual("-" , foo[4]);
            Assert.AreEqual("/" , foo[5]);
            Assert.AreEqual("+" , foo[6]);
        }

        [TestMethod]
        public void ValidationTest1()
        {
            var infixExpression = "9,9998+24/(7,8-3)";
            var foo = ShuntingYard.ValidateInfixExpression(infixExpression);
            Assert.IsFalse(foo.Any());
        }

        [TestMethod]
        public void ValidationTest2()
        {
            var infixExpression = "9,9998+24/(7,8-3";
            var foo = ShuntingYard.ValidateInfixExpression(infixExpression);
            Assert.IsTrue(foo.Any());
            Assert.AreEqual("The count of ( and ) tokens must match", foo.FirstOrDefault());
        }

        [TestMethod]
        public void ValidationTest3()
        {
            var infixExpression = "9,9998+24v/(7,8-3)";
            var foo = ShuntingYard.ValidateInfixExpression(infixExpression);
            Assert.IsTrue(foo.Any());
            Assert.AreEqual("Expression contains invalid characters", foo.FirstOrDefault());
        }

        [TestMethod]
        public void ValidationTest4()
        {
            var infixExpression = "+24v/(7,8-3)";
            var foo = ShuntingYard.ValidateInfixExpression(infixExpression);
            Assert.IsTrue(foo.Any());
            Assert.AreEqual(3, foo.Count);
        }


        [TestMethod]
        public void ValidationTest5()
        {
            var infixExpression = "9,9998+/7,8-3)";
            var foo = ShuntingYard.ValidateInfixExpression(infixExpression);
            Assert.IsTrue(foo.Any());
            Assert.AreEqual(3, foo.Count);
        }
    }
}
