﻿using System.Collections.Generic;
using Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class RpnTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var rpnExpression = new List<string>
            {
                "9",
                "24",
                "7",
                "3",
                "-",
                "/",
                "+"
            };

            var foo = Rpn.Eval(rpnExpression);

            Assert.AreEqual(15, foo);
        }
    }
}
