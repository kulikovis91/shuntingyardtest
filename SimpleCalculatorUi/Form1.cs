﻿using System;
using System.Windows.Forms;
using Autofac;
using Common;

namespace SimpleCalculatorUi
{
    public partial class Form1 : Form
    {
        private ISimpleCalculator _simpleCalculator;

        public Form1()
        {
            InitializeComponent();
            _simpleCalculator = Program.Container.Resolve<ISimpleCalculator>();
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            var expr = textBox1.Text;
            try
            {
                richTextBox1.Text += String.Format("The answer is: {0}\r\n", _simpleCalculator.Eval(expr));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
