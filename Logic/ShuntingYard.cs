﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Logic
{
    public static class ShuntingYard
    {
        private static readonly List<Operator> Operators = new List<Operator>
        {
            new Operator
            {
                Priority = 0,
                Value = "+",
                Type = OperatorType.Binary
            },
            new Operator
            {
                Priority = 0,
                Value = "-",
                Type = OperatorType.Binary
            },
            new Operator
            {
                Priority = 1,
                Value = "*",
                Type = OperatorType.Binary
            },
            new Operator
            {
                Priority = 1,
                Value = "/",
                Type = OperatorType.Binary
            },
            new Operator
            {
                Priority = 0,
                Value = "(",
                Type = OperatorType.Other
            },
            new Operator
            {
                Priority = 0,
                Value = ")",
                Type = OperatorType.Other
            }
        };

        public static List<string> ValidateInfixExpression(string expr)
        {
            var res = new List<string>();
            var splitExpr = expr.ToCharArray();

            if (splitExpr.Count(c => c == '(') != splitExpr.Count(c => c == ')'))
                res.Add("The count of ( and ) tokens must match");
            if (!Regex.Match(expr, String.Format("^[0-9,{0}]+$", String.Join("", Operators.Select(op => "\\" + op.Value)))).Success)
                res.Add("Expression contains invalid characters");

            for (int i = 0; i < splitExpr.Length; i++)
            {
                var curr = splitExpr[i];                

                if (Operators.FirstOrDefault(op => op.Type == OperatorType.Binary && op.Value == curr.ToString()) != null)
                {                    
                    if ((i - 1) >= 0 && (i - 1) <= splitExpr.Length)
                    {
                        int val;
                        if (!Int32.TryParse(expr[i - 1].ToString(), out val) && expr[i - 1] != '(' && expr[i - 1] != ')')
                            res.Add(String.Format("Missing operand to the left of {0} at {1}", curr, i));
                    }                    
                    if ((i + 1) >= 0 && (i + 1) <= splitExpr.Length)
                    {
                        int val;
                        if (!Int32.TryParse(expr[i + 1].ToString(), out val) && expr[i + 1] != '(' && expr[i + 1] != ')')
                            res.Add(String.Format("Missing operand to the right of {0} at {1}", curr, i));
                    }
                    if (i - 1 < 0)
                        res.Add(String.Format("Missing operand to the left of {0} at {1}", curr, i));
                    if (i + 1 > splitExpr.Length)
                        res.Add(String.Format("Missing operand to the right of {0} at {1}", curr, i));
                }
            }            

            return res;
        }

        public static List<string> Shunt(string infixExpression)
        {
            var outQueue = new Queue<string>();
            var opsStack = new Stack<string>();

            var errors = ValidateInfixExpression(infixExpression);

            if (errors.Any())            
                throw new Exception(String.Join("; ", errors));            

            var input = Regex.Split(infixExpression, String.Format("([{0}])", String.Join("", Operators.Select(op => "\\" + op.Value)))).Where(s => !String.IsNullOrEmpty(s)).ToList();
            
            foreach (var c in input)
            {                
                double val;
                if (Double.TryParse(c, out val))
                {
                    outQueue.Enqueue(c);
                }
                else if (c == "(")
                {
                    opsStack.Push(c);
                }
                else if (c == ")")
                {
                    while (opsStack.Any() && opsStack.Peek() != "(")
                    {
                        outQueue.Enqueue(opsStack.Pop());
                    }
                    if (opsStack.Any() && opsStack.Peek() == "(")
                        opsStack.Pop();
                }
                else if (Operators.Any(op => op.Value == c))
                {
                    var currentOp = Operators.FirstOrDefault(op => op.Value == c);
                    while (opsStack.Any() && Operators.FirstOrDefault(op => op.Value == opsStack.Peek() && op.Type == OperatorType.Binary)?.Priority >= currentOp?.Priority)
                    {
                        outQueue.Enqueue(opsStack.Pop());
                    }
                    opsStack.Push(c);
                }                
            }
            while (opsStack.Any())
            {
                outQueue.Enqueue(opsStack.Pop());
            }

            return outQueue.ToList();
        }
    }

    public enum OperatorType
    {
        Unary,
        Binary,
        Other
    }

    public class Operator
    {
        public string Value { get; set; }
        public int Priority { get; set; }
        public OperatorType Type { get; set; }
    }
}
