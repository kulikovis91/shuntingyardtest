﻿using System;
using Common;

namespace Logic
{
    public class SimpleCalculator : ISimpleCalculator
    {
        public string Eval(string expr)
        {
            // validate expr
            if (expr.Length > 10)
                throw new Exception("The expression must be 10 or less characters long.");
            var res = Rpn.Eval(ShuntingYard.Shunt(expr.Replace(" ", "")));
            return res.ToString("F1");
        }
    }
}
