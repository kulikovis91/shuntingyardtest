﻿using System;
using System.Collections.Generic;

namespace Logic
{
    public static class Rpn
    {
        public static double Eval(List<string> input)
        {
            var stack = new Stack<double>();

            foreach (var token in input)
            {
                double val;
                if (Double.TryParse(token, out val))
                {
                    stack.Push(val);
                }
                else if (token == "+")
                {
                    var opnd1 = stack.Pop();
                    var opnd2 = stack.Pop();
                    stack.Push(opnd1 + opnd2);
                }
                else if (token == "-")
                {
                    var opnd1 = stack.Pop();
                    var opnd2 = stack.Pop();
                    stack.Push(opnd2 - opnd1);
                }
                else if (token == "*")
                {
                    var opnd1 = stack.Pop();
                    var opnd2 = stack.Pop();
                    stack.Push(opnd1 * opnd2);
                }
                else if (token == "/")
                {
                    var opnd1 = stack.Pop();
                    var opnd2 = stack.Pop();
                    stack.Push(opnd2/opnd1);
                }
            }

            return stack.Pop();
        }
    }
}
