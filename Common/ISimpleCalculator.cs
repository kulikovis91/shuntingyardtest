﻿namespace Common
{
    public interface ISimpleCalculator
    {
        string Eval(string expr);
    }
}
